import React from 'react';
import GatsbyLink from 'gatsby-link';
import PropTypes from 'prop-types';

const BulletListTags = ({ tags, draft }) => (
  <div>
    <ul className="tags">
      {tags &&
        tags.split(',').map((tag, index) => (
          <li key={index}>
            <GatsbyLink to={`/tag/${tag}`}>{tag}</GatsbyLink>
          </li>
        ))}    
    </ul>
</div>
);

BulletListTags.propTypes = {
  tags: PropTypes.string,
  draft: PropTypes.bool,
};

export default BulletListTags;