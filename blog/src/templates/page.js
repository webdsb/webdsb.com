export default function Template({
    data 
  }) {
    const post = data.markdownRemark; 
    return (
      <div className="blog-post-container">
       <Helmet title={`webdsb - ${post.frontmatter.title}`} />
       {/* <div>{post.frontmatter.tags}</div> */}
        <div className="blog-post">
          <h1>{post.frontmatter.title}</h1>
          <div
            className="blog-post-content"
            dangerouslySetInnerHTML={{ __html: post.html }}
          />
        </div>
      </div>
    );
  }