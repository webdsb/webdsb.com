// import React from "react";
// import Helmet from 'react-helmet';
// import PropTypes from "prop-types";

// // Components
// import Link from "gatsby-link";
// import Pagination from '../components/TagsPagination';

// const Tags = ({ pathContext, data }) => {
//   const { tag } = pathContext;
//   const { edges, totalCount } = data.allMarkdownRemark;
//   const tagHeader = `${totalCount} post${
//     totalCount === 1 ? "" : "s"
//   } tagged with "${tag}"`;

//   return (
//     <div>
//       <h1>{tagHeader}</h1>
//       <ul>
//         {edges.map(({ node }) => {
//           const { path, title } = node.frontmatter;
//           return (
//             <li key={path}>
//               <Link to={path}>{title}</Link>
//             </li>
//           );
//         })}
//       </ul>
//       {/*
//               This links to a page that does not yet exist.
//               We'll come back to it!
            
//       <Link to="/tags">All tags</Link>*/}
//     </div>
//   );
// };

// Tags.propTypes = {
//   pathContext: PropTypes.shape({
//     tag: PropTypes.string.isRequired,
//   }),
//   data: PropTypes.shape({
//     allMarkdownRemark: PropTypes.shape({
//       totalCount: PropTypes.number.isRequired,
//       edges: PropTypes.arrayOf(
//         PropTypes.shape({
//           node: PropTypes.shape({
//             frontmatter: PropTypes.shape({
//               path: PropTypes.string.isRequired,
//               title: PropTypes.string.isRequired,
//             }),
//           }),
//         }).isRequired
//       ),
//     }),
//   }),
// };

// export default Tags;

// export const pageQuery = graphql`
//   query TagPage($tag: String) {
//     allMarkdownRemark(
//       limit: 2000
//       sort: { fields: [frontmatter___date], order: DESC }
//       filter: { frontmatter: { tags: { in: [$tag] } } }
//     ) {
//       totalCount
//       edges {
//         node {
//           frontmatter {
//             title
//             path
//           }
//         }
//       }
//     }
//   }
// `;

import React from 'react';
import PropTypes from 'prop-types';

import Posts from '../components/Posts';
// import Menu from '../components/Menu';
import Pagination from '../components/TagsPagination';
// import Separator from '../components/Separator';
// import MetaTags from '../components/MetaTags';

export default function Tags({ pathContext, data }) {
  const { siteUrl } = data.site.siteMetadata;
  const { posts, tag, pagesSum, page } = pathContext;

  return (
    <section className="main-content">      
      <section className="blog container tags-collection">
        <div className="medium-8 medium-offset-2 large-10 large-offset-1">
          <header className="header">
            <h1 className="tag-title tag-page-title">{tag}</h1>
          </header>
          <section className="tag-meta">{posts.length} posts in {tag}</section>

          <div className="posts">
          <Posts posts={posts} />
            {/* <Pagination page={page} pagesSum={pagesSum} tag={tag} />
            <Separator />
            
            <Separator />
            <Pagination page={page} pagesSum={pagesSum} tag={tag} /> */}
          </div>
        </div>
      </section>
    </section>
  );
}

Tags.propTypes = {
  data: PropTypes.object,
  pathContext: PropTypes.object,
};

export const tagsQuery = graphql`
  query TagsSiteMetadata {
    site {
      siteMetadata {
        title                
      }
    }
  }
`;