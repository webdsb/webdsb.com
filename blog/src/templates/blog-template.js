import React from "react";
import Helmet from "react-helmet";
import BulletListTags from '../components/BulletListTags';
import CommaSeparatedTags from '../components/CommaSeparatedTags';

export default function Template({
  data 
}) {
  const post = data.markdownRemark; 
  
  
  return (
    <div className="blog-post-container">
     <Helmet title={`webdsb - ${post.frontmatter.title}`} />
     {/* <div>{post.frontmatter.tags}</div> */}
      <div className="blog-post">
        <h1>{post.frontmatter.title}</h1>
        <h6>{post.frontmatter.date}</h6>
        <BulletListTags tags={post.frontmatter.tags}/>
        <br />
        <br />
        <br />
        <div
          className="blog-post-content"
          dangerouslySetInnerHTML={{ __html: post.html }}
        />
      </div>
      <CommaSeparatedTags tags={post.frontmatter.tags} />
    </div>
  );
}

export const pageQuery = graphql`
  query BlogPostByPath($path: String!) {
    markdownRemark(frontmatter: { path: { eq: $path } }) {
      html
      frontmatter {
        date(formatString: "MMMM DD, YYYY")
        path
        title
        tags
      }
    }
  }
`
;