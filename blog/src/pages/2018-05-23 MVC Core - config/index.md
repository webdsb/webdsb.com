---
path: "/mvc-configsettings/"
date: "2018-05-23T18:30:33.962Z"
title: "Retrieving settings in ASP.NET Core"
tags: Dot Net,ASP.NET MVC
---

When Microsoft moved from ASP.NET MVC to the current ASP.NET Core MVC there were many significant changes, including the removal of the web.config file.  This file was replaced with appsettings.json.

 
![Appsettings.json in solution explorer](../images/2018-05-23/appsettings_solution_explorer.png "AppSettings.json in solution explorer")

Accessing the web.config file from your MVC application was simple and straightforward:

`ConfigurationManager.AppSettings["MySetting"]`

We would simply reference the configuration manager object and retrieve the appsetting(s) we needed.  To further simply things you could reference the configuration manager and in turn the web.config file from any project within your solution - but is this a good thing?  The web.config and it's corresponding settings should be the UI or presentation layer's concern - it then would manage passing the information to sub layers.  In a full MVC application do we want our MSSQL repository referencing the web.config file directly?  Have we violated separation of concerns?  If we decided one day to no longer store our connection strings in the web.config the amount of code and files we would have to touch would potentially be large.

While .NET Core moves away from the web.config file for many reasons, the new infrastructure core brings a long allows us to easily use [dependency injection](https://stackoverflow.com/questions/14301389/why-does-one-use-dependency-injection/ "Dependency injection on stackoverflow") (DI) to retrieve our settings  By using DI we can then separate the retrieval of the settings from the actual use of the settings.  Do note, all this is possible with the web.config, but the practice of using DI in this scenario is not as forced as it is in the new .NET core world.

Let's get started.

We are going to open Visual Studio 2017 and create a new .NET Core MVC web application.
![New MVC Project using Core](../images/2018-05-23/new_project.png "New MVC Project using Core")
![New MVC Project using Core - MVC Select](../images/2018-05-23/mvc_proj.png "New MVC Project using Core - MVC Select")

Navigate to solution explorer and open up the appsettings.json - while different from the webconfig's xml layout, the same basic setup works in the appsettings.jso - it's just a json format.  Let's add a couple of settings:

```
{
  "AppSettings": {
    "Setting1": "This is a test",
    "Setting2": "1"
  },
  "Logging": {
    "IncludeScopes": false,
    "LogLevel": {
      "Default": "Warning"
    }
  }
}
```

Simple enough, we've added the AppSettings block.  Now we need to set up how these configuration settings are going to get injected.  Navigate to the the startup.cs file.  This is file is basically equivalent to the old global.asx in the MVC 5 and earlier versions.  It's the initial called class and most request will at some point filter through this class.  It is also important in that it sets up the container for our DI  work.  If you are not familiar with DI, at the highest level, the container will organize and manage all the classes we want to inject.  In startup.cs we set these up in the ConfigureServices method.  Out of the box your ConfigureServices probably looks like this:
```
public void ConfigureServices(IServiceCollection services)
{   
    services.AddMvc();   
}
```

We are going to add a service for our configuration item - because Microsoft knew the settings would be a much used piece of functionality they gave us an IConfiguration class that we can use as our interface.  Let's add a property to our class and an additional service to our ConfigureServices
```
public IConfiguration CustomConfiguration { get; }

public void ConfigureServices(IServiceCollection services)
{   
    services.AddMvc();
    services.AddSingleton<IConfiguration>(CustomConfiguration);
}
```

Now, what's going on?  We've created a simple property called CustomConfiguration that will be injected into our controller's construction - this is called constructor injection.  Next we added a singleton to our services.  You could write the services.AddSingleton...out fully like below:

```services.Add(new ServiceDescriptor(typeof(IConfiguration), typeof(IConfiguration), ServiceLifetime.Singleton));```

But thankfully Microsoft has given us a few helpers and shortcuts.  We can create three types of injectables:

**Singleton** will create a single instance and that instance will be shared across all objects/classes/etc and the request.
```services.AddSingleton<IConfiguration>(CustomConfiguration);```

**Scoped** will create a new instance on every new request - so every new request will get a new instance.
```services.AddScoped<IConfiguration>(CustomConfiguration);```

**Transient** are created every time they are needed.
```services.AddTransient<IConfiguration>(CustomConfiguration);```

Now that our container has been set up, we are ready to access our IConfiguration class through our controller's constructor.  Let's navigate to the pre-built HomeController that came with our MVC project and add a couple of properties and a constructor:
```
private string setting1 { get; set; }
private string setting2 { get; set; }

public HomeController(IConfiguration configuration)
{
    setting1 = configuration["AppSettings:Setting1"];
    setting2 = configuration["AppSettings:Setting2"];
}
```

And to test our changes, just use one of the Actions (like About or Contact) to show your settings on the view...I chose the About action:

```
public IActionResult About()
{
    ViewData["Message"] = $"Setting1: {setting1} and Setting2: {setting2}";

    return View();
}
```

Just like that we have pulled our settings over to the controller using DI.
![About Page](../images/2018-05-23/results.png "Successful About Page")

This is just a start - but hopefully you can begin to see how this can be cleaned up further and more separation can be had.  In addition with a DI container now packaged with Core, getting started with dependency injection has never been this easy in .NET.  

You can reference my source code here: [SimpleMVCCore](https://gitlab.com/webdsb/SimpleMVCCore "Simple MVC Core project")