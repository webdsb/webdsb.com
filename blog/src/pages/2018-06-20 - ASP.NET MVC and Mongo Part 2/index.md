---
path: "/mongo-and-mvc-core-part2/"
date: "2018-06-20T21:30:33.962Z"
title: "Connecting ASP.NET MVC and Mongo Part 2"
tags: Mongo,Dot Net,ASP.NET MVC
---

Last week we talked about what [Mongo](http://webdsb.com/mongo-and-mvc-core/ "Mongo and .NET Core") is and how to set up our first Mongo Database using [MLAB](https://mlab.com "MLAB Mongo"). Now that you are caught up let's get our [SimpleMVCApp](http://webdsb.com/mvc-configsettings/ "Simple Asp.NET MVC Core app") up running with our Mongo database.

We will start by adding a Data Access Layer project to our solution.  In this simple example the DAL project will hold all our code to interact with the database as well as the POCOs (Plain old CLR objects) or domain objects.

![New DAL Project](../images/2018-06-13/Add_Project.png "New DAL Project")

With our project set up we now want to add the mongo db drivers and mongo db library for .NET CORE.  These will help make interacting, querying, and working the BSON objects of mongo much easier.  In the nuget browse window search for MongoDB.Driver - the main nuget package will handle all our dependencies.  At the time of this post, the latest stable release is [2.6.1](https://docs.mongodb.com/ecosystem/drivers/csharp/ "MongoDB Driver")

![Nuget Project](../images/2018-06-13/Nuget_Package.png "Nuget Project")

Create a new class inside our DAL project and let's add some code:

```
using MongoDB.Driver;
using System;

namespace DAL
{
    public class Animal
    {
        private MongoClient Client { get; set; }
        private IMongoDatabase Database { get; set; }

        public Animal(string connectionString)
        {
            Client = new MongoClient(connectionString);
            Database = Client.GetDatabase("simplemvcproject");
        }
    }
}
```
To dissect what's going on - we have created a class called Animal and created two properties related to MongoDB.  The MongoClient is the concrete class that will set up all connection properties.  It handles the connection open and close to our server.  The IMongoDatabase is an interface to the actual database tools.  We will use our Client to retrieve a database and the IMongoDatabase class will handle all CRUD operations.

We will need an object to hold our data - will create an AnimalDomain class and an Attribute class.  These classes will hold the values from our query.
```
public class Attribute
{
    public int legs { get; set; }
    public bool tail { get; set; }
}
```
```
public class AnimalDomain
{
    public string name { get; set; }
    public string[] colors { get; set; }
    public Attribute attributes { get; set; }
}
```
At this point we are ready to query our database using the mongo driver.  Inside our DAL class create a method called GetAnimals.  This method will handle the retrieval of our animals collection.
```
public IEnumerable<AnimalDomain> GetAnimals()
{            
    return Database.GetCollection<AnimalDomain>("Animals").AsQueryable();            
}
```

There is a lot of magic that the mongo library is handling here for us.  It is reaching out to our collection, pulling the data back, and then mapping it to an Enumerable of type "Animal".  The mapping happens because our field names match the property names of our class.  With our method to get the data back from the collection, we need to make our way back to our home controller and add some properties to get our connection string.

```
 public class HomeController : Controller
{
    private string setting1 { get; set; }
    private string setting2 { get; set; }
    private DAL.Animal AnimalDAL { get; set; }

    public HomeController(IConfiguration configuration)
    {
        setting1 = configuration["AppSettings:Setting1"];
        setting2 = configuration["AppSettings:Setting2"];
        AnimalDAL = new DAL.Animal(configuration["MongoConnection:ConnectionString"]);
    }
    .
    .
    .
```
All we have done here is create a reference to our DAL project and pass in the connection string from the appsettings.json.  A few weeks back we talked about how to [retrieve settings from an MVC Core app](http://webdsb.com/mvc-configsettings/ "Simple MVC App settings").  Make sure you update your appsettings to hold the mongo connection string.
```
{
  "MongoConnection": {
    "ConnectionString": "mongodb://userid:password@server:port/db"    
  },
  "AppSettings": {
    "Setting1": "This is a test",
    "Setting2": "1"
  },
  "Logging": {
    "IncludeScopes": false,
    "LogLevel": {
      "Default": "Warning"
    }
  }
}
```

Finally pick a controller and simply add a call to our DAL method GetAnimals.  For simplicity sake I am going to continue to use the prebuilt MVC about page and continue updating our growing about message.
```
public IActionResult About()
    {
        var data = AnimalDAL.GetAnimals().ToList().FirstOrDefault();
        ViewData["Message"] = $"Setting1: {setting1} and Setting2: {setting2}.  Animal is {data.name} and has {data.attributes.legs} legs";

        return View();
    }
```
And just like that we have pulled back the animal name and the number of legs.
![Results](../images/2018-06-13/Results.png "Results")

This is an overly simple project with a simple connection.  But you should quickly see how the mongo library allows us the use .NET Linq to query our mongo database.  We very easily could have update our GetCollection as below.
```
return Database.GetCollection<AnimalDomain>("Animals").Find(x => x.userID == userName).AsQueryable();
```
or
```
return Database.GetCollection<AnimalDomain>("Animals").Where(x => x.userID == userName).First().AsQueryable();
```

We can now simply our database logic all into simple linq queries - no need for a separation of database query and client linq queries, it is all handled the same.

Hopefully this will jump start your mongo and .NET core app - if you need the source code you can find it [here](https://gitlab.com/webdsb/SimpleMVCCore "SimpleMVCCore").