---
path: "/the-retrospective/"
date: "2018-08-08T23:15:33.962Z"
title: "The Retrospective"
tags: Agile
---

This is the 4th part of a series on agile...catch up here:  
[Part 1 -The User Story](/the-user-story/ "Part 1 - The User Story")  
[Part 2 - The Requirements](/the-requirements/ "Part 2 - The Requirements")  
[Part 3 - The Stand Up](/the-standup/ "Part 3 - The Stand Up")  

If you are doing scrum development or agile and not doing a retrospective you really are not doing a true agile development cycle.  The retrospective is crucial to a successful agile development team.  The retrospective occurs at the end of the development cycle, typically before the start of the next development iteration.  

The retrospective is a chance for the development team to come together and discuss the previous development iteration.  The scrum guide says this meeting should be "positive and productive".  It is not a time to air dirty laundry or blame failures on an individual.  Instead the development team can discuss what issues they had in a more general sense.  Instead of placing blame, get to the bottom of why something was not completed or why a particular task took longer than it should.  If an individual is causing issues, the development team can use the retrospective to take corrective action - but again it should be handled with kindness.  The scrum master should keep a list of items the team needs to improve upon and in following retrospectives the list should be discussed and improvements noted.

The retrospective should not only be negative.  It is also a great time for the team to speak about successes - the things that have gone well.  If there are items that the team is really functioning well on, bring those to light and make sure the team keeps doing it.  

The retrospective is key to continual improvement and adaptation.  While it can seem difficult to discuss tough situations, it is important to the team's success that they be willing to improve.


