---
path: "/vs_code_extensions/"
date: "2018-10-03T23:15:33.962Z"
title: "Visual Studio Code Extensions"
tags: Musings
---

[Visual Studio Code](https://code.visualstudio.com/) is a great open source cross platform code editor that is also much lighter than a full [Visual Studio](https://visualstudio.microsoft.com/) install.  While developing [ASP.NET MVC](http://www.webdsb.com/tag/Dot%20Net) applications I like to use Visual Studio, but for website development, this blog, [python](http://www.webdsb.com/tag/Python) applications, node, and many others I prefer the lighter, quicker Visual Studio Code.  While developing in Visual Studio Code, I have found several extensions that have proven extremely beneficial in working with Visual Studio Code.

####JS Linter
For anyone doing heavy JavaScript a linter is a huge bonus to development - it allows you to essentially "compile" and "check" your code before deploying.  It can show you errors and possible issues that could arise (see using === or ==).  I like the [JS "Standard" Linter](https://marketplace.visualstudio.com/items?itemName=shinnn.standard) based off the JS standard.  The JS standard follow some common conventions the JavaScript community follows.  See the full list of [JavaScript Standards](https://github.com/standard/standard).  The linter helps you develop quickly because it does a live check of your code as you develop catching mistakes and issues.  A linter won't catch every error or potential error, but can act as a second set of eyes for you JavaScript.

####Chrome Debugger
Another handy JavaScript extension is the [Chrome Debugger](https://marketplace.visualstudio.com/items?itemName=msjsdiag.debugger-for-chrome).  It allows you to actually execute your JavaScript from within VS Code through Chrome and debug inside VS Code.  This is handy as it doesn't require you to attempt to use the F12 developer tools and jump back to VS Code to make any changes.  In addition the Chrome Debugger provides a watch window to show variable and statement values and a console that allows you to live code while your JavaScript is running.

####Intellisense
One of things often missing from JavaScript or Python development is intellisense.  Visual Studio Code has plenty of extensions that support different languages and their intellisense, from Python to Node to C#.  For react development, the [React Native Tools](https://marketplace.visualstudio.com/items?itemName=vsmobile.vscode-react-native) is necessary.  It provides intellisense, as well as a fully react-native command tool so you can run the react commands while debugging your react app.

####Git
VS Code comes with git tools installed.  It is just a base install and provides the common commands.  [GitLens](https://marketplace.visualstudio.com/items?itemName=eamodio.gitlens) provides some additional capabilities.  It provides excellent compare and diff tools while increasing the ability to search commits and history.  If you do heavy git work, this is a must have.

####File/Folder Compare
I use VS code as a basic text editor as well.  One of things most developers find themselves needing to do at some point is compare files or folders.  [Folder Compare](https://marketplace.visualstudio.com/items?itemName=otto77.folder-compare) is an excellent tool for doing that.  Simply open up folder or file in VS Code, right click it and instantly see the difference in the two.

These are just a few of my favorite extensions, whenever I do a new VS Code install, I always make sure these extensions are installed.  What are some of your favorites?