---
path: "/agile-blog-development/"
date: "2018-06-06T21:15:33.962Z"
title: "Blogging with Agility"
tags: Musings,Blogging,Agile
---

###Blogging

I am new to blogging and want to keep a consistent blogging schedule, while also maintaining regular site updates.  To do this I plan to run weekly blog sprints.  Since this blog is not a full time job, what does a weekly blog sprint look like?

####Trello
I plan to run my site using a [kanban](https://en.wikipedia.org/wiki/Kanban/ "Kanban") methodology; I will write more about kanban later, but for now and at a thousand foot view, kanban is simply an agile way to develop software that puts an emphasis on continually delivering something. Kanban uses a board and cards - in fact kanban is Japanese for card - to organize work in an easy to visualize way.  Kanban forces an evolutionary approach to software development - instead of knowing all the answers up front, we simply evolve over enough sprints until we approach peak.  A key component to kanban is Work In Progress or WIP. Kanban puts a limit on the WIP meaning a team sets the maximum amount of work in the in progress lane.  I'll point out my WIP below.

I have set up two kanban boards on [Trello](https://trello.com/ "Trello").  

One board is for posts:
![Blog Post Board](../images/2018-06-06/Blog_Post_Board.png "Blog Post Board")

Using this board, the ideas lane can be a quick place to jot notes and blog post ideas.  They can be as short as a single word or a more detailed list of notes.  There is no limit to the amount of cards here.  Once I decide to start working on an idea I move it to the drafts swim lane.

![Blog Post Board](../images/2018-06-06/Blog_Post_To_Draft.png "Blog Post Board")

Once a card is in this lane, I will actually start writing out the draft - this includes both the blog text, research, and the markdown to style the post.  I have set my WIP to two post in draft.  This may evolve over time, but this allows me to narrow my focus and not get into too much drift. If over time I see more or less capacity this WIP can be adjusted up or down as needed. Once a post feels completed, I will move it to the Testing lane.

![Blog Post Board](../images/2018-06-06/Blog_Post_To_Test.png "Blog Post Board")

A post in the testing lane will be reviewed for any issues both in the blog text and in the layout of the page.  Any failures will result in the post moving back to Drafts; successes will move forward to published.

![Blog Post Board](../images/2018-06-06/Blog_Post_To_Publish.png "Blog Post Board")

A post in the published lane will be published with the next build of the site.

I have used a similar approach to site updates and style changes - but have used the more traditional kanban swim lanes (ToDo, InProgress, Testing, Done).
![Blog Site Board](../images/2018-06-06/Blog_Site.png "Blog Site Board")

This is a separate board and allows blog posts and site changes to move independently of each other.  I can work on weekly blog posts, but perhaps site updates will run on a monthly sprint.  Again I am keeping my WIP to two cards.

###Pomodoro Technique
The [Pomodoro Technique](https://francescocirillo.com/pages/pomodoro-technique "Pomodoro") is a time management strategy that uses a tomato time to break work down into "pomodoros" or 25 minute work sprints.  After a 25 minute work sprint you take a 5 minute break.  Once you have completed 4 pomodoros you take a 15 minute break.  The idea behind a pomodoro is that you can focus full on for 25 minutes and ignore any interruptions.  During a pomodoro you will make note of any interruptions and determine how you can limit and reduce those in the future.  If you are unable to ignore an interruption you must abandon the pomodoro and resume your count with the next full pomodoro.  The name pomodoro comes from the low tech tomato timers the inventor of the method used to time his pomodoros.  Not wanting to carry a tomato timer I have used the very simple timer called [Marinara Timer](https://www.marinaratimer.com/IiPyOIM7 "Marinera Timer").  It is a simple timer, that is free and browser based so it works on all computers and mobile devices.

###Fail Fast and Adapt
Finally I subscribe to a fail fast methodology.  If a post isn't working out, or a layout change doesn't look right, I move on and adjust.  Perhaps a post just isn't coming together, it can stay in the draft lane or move back to the ideas lane.  If a site layout change does not look or feel right, roll back and change.  The beauty of an agile approach is there is always another sprint.

As with any good agile process this is just the start - this will evolve and change over time.  This is the process, tomorrow hopefully it will improve.