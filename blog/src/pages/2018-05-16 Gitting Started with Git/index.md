---
path: "/git-started/"
date: "2018-05-16T17:30:33.962Z"
title: "Gitting Started With Git"
tags: Git
---
I want to start out by saying I come from a non distributed source control background like Team Foundation Server.  Git is something I have only played with on the surface.  I am writing this post for me – since I don’t use git everyday on the job, remembering my commands and flow is a challenge – this will give me a quick place to come back for reference.  

One of the most frequent workflows for me is, create my initial code directories local and then come back later and add my source control.  Adding git to an existing directory is very easy.

### New Repository
I will start by assuming you have created your remote repository on some hosted git solution – in my case I am using [GitLab](http://gitlab.com/ "GitLab's website").

The first step is to open your favorite shell (PowerShell in my case) and change to your local code directory.  Once that is done, it's time to intialize the local repository for git.

`PS D:\Code\TestProject> git init`

Once we have linked our local directory to git, we can add a remote origin so our code history does not live on our local machine.

`PS D:\Code\TestProject> git remote add origin git@gitlab.com:webdsb/TestProject.git`

We now have an empty git repository with a remote repository, but no code.  Since this repository is completely empty I will simply add all files in the local directory.

`PS D:\Code\TestProject> git add .`

I could have specified individual files or directories here - the '.' will include all files not present in the `.gitignore` file.  Now I am ready to commit the changes to my local repository.

`PS D:\Code\TestProject> git commit -m "My first git commit"`

Finally we can push the entire commit to our master branch

`PS D:\Code\TestProject> git push -u origin master`

I could have pushed to any remote branch that existed - in this case I just used the default master branch.

At this point, perhaps I have forgotten a file I need to add.  My projects needs the readme markdown file so other developers/users can understand what my project is doing.  I am going to go ahead and add it to the master branch.  

First I am going to go ahead and ensure I am on the master branch.
`PS D:\Code\TestProject>git checkout master`

Once on the master branch, I can simply repeat the above steps instead of adding all files, I will only add the single file I want.
`PS D:\Code\TestProject> git add .\readme.md`

With the file added to my local repository, I will commit the change to my local repository.
`PS D:\Code\TestProject>  git commit -m "Added the readme file`.

Finally lets not forget to push the change to our remote repository.
`PS D:\Code\TestProject> git push`

That is it - we were able to initialize an existing directory to be git aware and then push all the files up to our remote directory.  After our initial add we were able to go back and add a file we missed - all done from the command line.  If you are more comfortable with a gui - git is fully integrated into [Visual Studio](https://www.visualstudio.com/ "Visual Studio") or [Visual Studio Code](https://code.visualstudio.com/, "Visual Studio Code website").  If you don't plan to develop with microsoft utilities you can browse the many open source [git guis](https://git-scm.com/download/gui/windows "git gui websites").
