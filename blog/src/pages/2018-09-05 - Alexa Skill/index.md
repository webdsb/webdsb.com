---
path: "/alexa-skill/"
date: "2018-09-05T18:30:33.962Z"
title: "Creating an Alexa Skill"
tags: Alexa,Python
---

Creating and developing Alexa skills is all the rage right now.  The small [echo dots ](https://www.amazon.com/gp/product/B01DFKC2SO/ref=as_li_tl?ie=UTF8&tag=webdsb-20&camp=1789&creative=9325&linkCode=as2&creativeASIN=B01DFKC2SO&linkId=3d723bc98700438f2a9208ca184c4ecc "echo dots") to the larger [echo show ](https://www.amazon.com/gp/product/B010CEHQTG/ref=as_li_tl?ie=UTF8&tag=webdsb-20&camp=1789&creative=9325&linkCode=as2&creativeASIN=B010CEHQTG&linkId=67f74440676aa15162fe62aedf680792 "echo show") are increasingly showing up in homes and the use cases for them continue to grow.  From ordering more peanut butter to controlling your lights, these little devices can do so much.

While Amazon has touted the Alexa Skill set to allow non developers the opportunity to create their own skills - to create a fully functional skill we are going to want to write some code.  So what do we need...
1. Python Script 
2. A Database 
3. A Cloud provider to host our script

Today, we are going to focus on our Skill and the Python script.

####The Skill
Our first skill will be something simple - we are going to ask Alexa to tell us something based on a key word...so perhaps something like this...
```
Alexa ask my test skill to tell us a story about trees.
```
Our python code will use that keyword to pull a story from the database and send the story text back to Alexa to read out loud to us.

The first thing we need to do to create our Alexa skill is to head over to [Amazon Developers](https://www.developer.amazon.com "Amazon Developers") and create a new skill.  Note everything we will be doing we will do from scratch.  Any skill you might want to write that fits one of Amazon's prior templates you would want to choose.  This will pre-fill some of the boiler plate and cut down on how much code and information you have to write.

![Create Skill ](../images/2018-09-05/AlexaSkillCreateName.png "Create Skills")

The first piece we will create is the intent - the intent is how a user will interact with your skill.  For example the intent is bolded:  _Alexa **tell me the weather**_.  The interaction with Alexa is tell me the weather - we are asking (or demanding) Alexa to tell us what the weather is.

![Create Intent ](../images/2018-09-05/AlexaSkillCreateIntent.png "Create Intent")

Once you have created the intent template, you can now add sample utterances...maybe
```
tell us a story about trees
```

You might be noticing an issue?  Our sample utterance is hard coded to _trees_.  What if we want a story about cars, or dogs, or some other keyword.  This is where slots come in, every intent can have 0 or more slots and it is simply a place to hold a variable, so maybe our example utterance will look this with a slot.

```
tell us a story about {story}
```

The {} tell the intent to capture the spoken word and we will then have access to that spoken word to do our database look up.

At this point we have enough of our skill built out that we can start developing our script.

####The Script
You can write your script in just about any language, the two most popular are python and nodejs.  The reason being both languages have huge open source support and therefore have many user created libraries to aid in rapid development of a skill.  For this example I chose python simply for how quick I can get python up and running (simply install the [python](https://flask-ask.readthedocs.io/en/latest/ "python") version you want and start writing code.  For .Net developers [Visual Studio](https://docs.microsoft.com/en-us/visualstudio/python/installing-python-support-in-visual-studio?view=vs-2017 "Visual Studio") comes with a set of tools for python development.).

For Python development the Flask tool set is a must - and now includes an Alexa library called Flask-Ask.  For a quick primer, Flask is a framework (i.e. a set of libraries) inside python that provide server frameworks, unit testing abilities, REST calls, and many other useful code snippets you don't want to recreate.  Add flask and the flask ask to your python script

```
from flask import Flask, render_template
from flask_ask import Ask, statement, question
```
These two lines give us both Flask and the alexa library, we want to use both the input (question) and output (statement) helpers so we've imported both.  Next we need to to tell flask_ask what the intent is it should look for.  When we've completed our python app, we will tell our Alexa skill what endpoint the python app is running at, when you call the python app it will then reach out and look for intents both in production and development, so be sure your intent in the python code matches your intent name you created above.

```
@ask.intent("MyTestSkillIntent")
```

For now let's just get Alexa reading us back a message.  First we will define an entry function and simply return a statement for Alexa to read back.  We will want to go ahead and set our function up to take an input - we just won't use it now.  Remember when we set up our intent we added a slot called story - this means that when we speak to Alexa for our skill, Alexa will pass us some piece of data.  We need that data in our input parameter.  So our function might look like this...

```
def Get_Story(story):    
    #We will eventually do some Database look ups here
    message = "Hi, this is my test skill intent";       
  
    return statement(message)
```

And just like that we have an entry function that our Alexa app can call into and get back a message.  I've skipped over the pieces of the python app that need to act like a server - this will vary based on whether you go with python and flask, just python, or if you choose Nodejs.  The full code in python might look like this...
```
__author__ = 'webdsb'

from flask import Flask, render_template
from flask_ask import Ask, statement, question

#Set up our app - this will do more once we are hitting a database
app = Flask(__name__)
#initialize Alexa library
ask = Ask(app, "/")

@ask.intent("MyTestSkillIntent")

#We will self host the python app as a server - this way our alexa skill can call to it.
@app.route('/', methods=['POST'])

def Get_Story(story):    
    #We will eventually do some Database look ups here
    message = "Hi, this is my test skill intent";       
  
    return statement(message)

#Server set up - will be important when we host
port = int(os.environ.get("PORT", 5000))
if __name__ == '__main__':
    app.run(debug=False, host='0.0.0.0', port=port)


```
Now we want to test our skill, but we are not quite ready to push the entire thing to a cloud provider or some other host.  Fortunately there are a few ways to get around this - we could host the app on a raspberry pi linux server locally or if we are just testing use a tool like [ngrok](https://ngrok.com/ "ngrok").  This will allow us to run our python app locally and have our Alexa skill call back to spoofed https link locally.  Again this is just developing, we wouldn't want to run a full production app from here!  Simply download ngrok and run the command

```
ngrok http 5000
```
Note the port in the command should match the port in your python code.  Now simply copy the https endpoint generated and paste it in the endpoint section of the alexa skill...

![Create Endpoint ](../images/2018-09-05/AlexaSkillCreateEndpoint.png "Create Endpoint")

Now we can test our skill 
```
Alexa ask my test skill to tell us a story about trees.

Hi, this is my test skill intent
```

Now that we have our script, next week we will add a database look up and deploy to a cloud server.