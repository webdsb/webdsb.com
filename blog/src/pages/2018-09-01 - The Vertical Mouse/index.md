---
path: "/jtech-vertical-mouse/"
date: "2018-09-03T18:30:33.962Z"
title: "The J-Tech Vertical Mouse"
tags: Reviews,Hardware
---

Years of mouse usage can take its toll on your body.  Even with perfect posture, using a mouse can increase the likelihood of developing carpal tunnel in the wrist of the hand you use the mouse with. In addition regular usage of a mouse can cause shoulder joint and muscle pain.  When using a standard mouse the arm, even in perfect posture, is put in a twisted position right above the wrist.  This twist increases pressure on the wrist and can lead to carpal tunnel syndrome.  In addition moving a standard mouse is done by using the wrist, small mouse movements will find you flipping your wrist left or right, again increasing the pressure.

####The vertical mouse
At first glance, the vertical mouse looks strange, a mix between a joystick, a mouse, and some kind of game controller.

 
![J-Tech Wired/Wireless Mouse](../images/jtech-mouse.png "J-Tech Wired/Wireless Mouse")

So what are the benefits of the vertical mouse?
* Position - the vertical mouse puts the arm in a natural rest position.  The body's arm's naturally sits with the hands facing in toward the body (the pinky finger down), not in a position where the palms face behind you.  The vertical mouse lets the arm stay in this position and removes the forced pressure of keeping the palm down.
* Arm movement - the benefit of the position is that any mouse movement is now done by the arm and not the wrist.  This removes almost all pressure and strain from the wrist.

####Trying a vertical mouse
To try a vertical mouse, I chose a middle of the road model.  There are cheaper mice with fewer features and more expensive ones with additional buttons and bluetooth.  I chose the [J-Tech Vertical USB mouse ](https://www.amazon.com/gp/product/B0759V6FZC/ref=as_li_tl?ie=UTF8&camp=1789&creative=9325&creativeASIN=B0759V6FZC&linkCode=as2&tag=webdsb-20&linkId=abc83f7e3feed9c40d881eefdb886eac "J-Tech Vertical USB Mouse") as my trial run with the vertical mouse.  The J-Tech comes in both a wired usb and wireless (with usb adapter) version.  I will be reviewing the wireless version.
####Comfort
When you first switch to a vertical mouse nothing will feel right.  It will feel like relearning to use a mouse, but the J-Tech provides an extremely comfortable grip and feel.  The palm rest is a soft rubberized compound and the button surround is a smooth plastic.  This makes adjusting to the new positions easier.  The position of the left and right mouse button are perfectly positioned for the so no actual movement is required to find them.  Moving between the scroll wheel and right mouse button feels natural and requires only finger movement, again minimizing wrist only movements.
####Features
The J-Tech mouse comes with 4 buttons and a scroll wheel.  The left right mouse buttons and scroll wheel function as expected, but a nice feature is the two additional buttons on the left side of the mouse - right where the thumb lands.  These buttons function as a back and forward button.  To minimize movement from the mouse, you can use the back and forward button navigate instead of moving the mouse up to an on screen back or forward button.  If you spend a lot of time web browsing, this feature will greatly reduce mouse movement.  It does take getting use to - I suspect most of us are use to moving our mouse up and using the browser's back and forward buttons.
The mouse also provides a DPI button to allow you to adjust the scroll smoothness.  If your scrolling feels laggy or to slick you can click through the DPI button until you find a scroll state that feels best to you.
####Overall
The J-Tech is an excellent mouse and a great entry point into the vertical mouse trial.  It provides the standard mouse features and adds the two handy back and forward buttons to help minimize hand movement.
