---
path: "/hello-world/"
date: "2018-05-09T20:01:33.962Z"
title: "Hello World - Gatsby"
tags: Musings,Gatsby

---
### Hi
Welcome to my corner of the internet.  I've built this website using the static website generator [Gatsby](https://www.gatsbyjs.org/ "Gatsby's Homepage").  Gatsby is a static site generator that uses [React](https://reactjs.org/ "React's Homepage") to build fast websites that do not rely on Content Management Systems.  With Gatsby you bring your own data source (Drupal, Wordpress, Markdown, or JSON to name a few) and the engine will generate your static html pages.

Getting started with Gatsby is super easy...all you need is Node.

Open your favorite shell and insall the gatsby tools.

`npm install --global gatsby-cli`

With gatsby now installed globally we can use the scaffolding and create a new site

`gatsby new my-blog`

Change to the directory just created

`cd my-blog`

Build/Compile and start the local node server

`gatsby develop`

You can now navigate to localhost:8000 and see the gatsby starter site.  It's a simple site with 2 pages, but should quickly show you the power and ease of use gatsby brings.

From this point you can alter and edit the design of your site by modifying the js files inside the src folder.  Some knowledge of React will be helpful, but for most common task it should be fairly simple to follow the existing code.  

Once you are ready to go live, simply run

`gatsby build`

Gatsby will build and optimize your site by minifying and removing un-needed resources.

As I learn more about Gatsby and continue to develop this site, I plan to share what I've learned and insights I come across.


