---
path: "/the-requirements/"
date: "2018-07-11T23:15:33.962Z"
title: "The Requirements"
tags: Agile
---

This is the 2nd part of a series on agile...catch up here:  
[Part 1 -The User Story](/the-user-story/ "Part 1 - The User Story")  
[Part 2 - The Requirements](/the-requirements/ "Part 2 - The Requirements")  
[Part 3 - The Stand Up](/the-standup/ "Part 3 - The Standup")  

Last week we talked about [the user story](/the-user-story/ "The User Story") and how it is integral to a successful sprint.  It should be simple, short, and full of information.  This week, let's talk about the Requirements.

Gone are the days of old where a developer was handed a multi page word document with paragraph after paragraph of information.  These multi page word documents were used by business analyst, developers, and QA testers to do their work.  Often because of the depth of information and length of the document, pieces of the requirement were missed.  This caused major set backs because in a waterfall development cycle the user doesn't see the application until the very end.  Anything missed would cause the development to start the development cycle over causing a loss of time and confidence.

In the agile world, the requirements are actually part of the acceptance criteria.  The acceptance criteria is a list of criteria that the code must meet to pass the Product Back Log item (PBI).  You will see acceptance criteria listed out in a numbered list or ordered list - but this tends to lead to acceptance criteria creeping back towards long paragraphs of requirements.  Instead, the Gherkin method for writing acceptance criteria can help us keep our criteria short and too the point.  Another benefit of using a method like Gherkin is that there are tools that can take your acceptance criteria and generate the test cases for the requirement.

####Gherkin
Gherkin follows a simple template:
```
Given <some scenario>
When <some action>
Then <result>
```

Your [user story](/the-user-story/ "The User Story") can have multiple of these Gherkin style acceptance criteria.  In our previous post we had a simple user story...

```
 As an administrator
 I need to enter customer addresses
 So I can keep records updated
 ```

 Now let's add some acceptance criteria to our user story.

 ```
 Given I need to update an address
 When I enter the address in the text box
 Then the submit button should enable
 ```

 ```
 Given I enter an address but change my mind
 When I press the cancel button
 Then clear the text in the address box.
 ```

 ```
 Given I am an administrator
 When I enter the address
 Then the text box should show the address in 10pt font
 ```
From a developer's stand point, we have clearly defined requirements here - we know when to enable the button, what happens when we cancel, and even the font.  There is definitely room for more criteria here, but even as we add more we don't see the same issues we had with the pages of requirements.  As a developer is working on their code they can easily walk through the requirements and make sure each acceptance criteria passes.  A QA tester can go through the criteria and develop the proper test cases.

One more advantage to this type of requirement for a developer is how easy a unit test is to derive off of this...notice
```
Given
When
Then
```
We can take our Given/When/Then and turn them into
```
Given = Arrange
When = Act
Then = Assert
```
We now have the basis for our unit test - and if you Test Driven Development is your practice, you can very easily get your unit test up and running before you touch your code.

Hopefully you can see the simplicity of the overall requirement - we've covered all the information that would have been in a normal requirement document, but have kept it easy to read, easy to follow, and placed the emphasis on what the user wants and needs.

