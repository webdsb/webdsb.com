import React from 'react'
import Link from 'gatsby-link'

const AboutPage = () => (
   <div className="content">
                <img className="aboutphoto" src="images/david.png" />
                <h5>Hi, I'm David</h5>
                <br />
                <h7>Developer, Technologist, Learner, Leader. I write code and create solutions.</h7>
                <br />
                <br />
                <p>I have spent the past several years developing applications across multiple platforms using a variety of languages.  From the desktop to the web and mobile I have had the opportunity to create software to solve many problems.</p>
                <p>I am currently a web developer in Chattanooga, Tennessee.  I develop web applications using ASP MVC/C#, JavaScript, HTML, and Bootstrap.  I enjoy playing with <a href="tag/React">React</a>, Angular, <a href="tag/Dot%20Net">.NET Core</a>, and <a href="tag/mongo">Mongo</a>.</p>
                <p>While I love writing code, I also enjoy reading/thinking/focusing on the practices of developing software.  The code is important, but how we develop and what methodologies we use impact the code and doing that right is important. My focus lately has been on <a href="tag/agile">Agile</a> practices and the roles we play in agile development.</p>
                <p>This site is created using <a href="/hello-world/">Gatsby</a>. I am building this site as I learn both <a href="https://www.gatsbyjs.org/"> Gatsby </a> and <a href="https://reactjs.org/">React</a>.  You can find the source <a href="https://gitlab.com/webdsb/webdsb.com">here.</a></p>
                <br />Feel free to contact me at <a href="mailto:david@webdsb.com">david@webdsb.com</a>
                <br />
                <br />
                <p>The opinions expressed here are mine.</p>
    </div>
)

export default AboutPage
