---
path: "/the-standup/"
date: "2018-07-30T23:15:33.962Z"
title: "The Standup"
tags: Agile
---

This is the 3rd part of a series on agile...catch up here:  
[Part 1 -The User Story](/the-user-story/ "Part 1 - The User Story")  
[Part 2 - The Requirements](/the-requirements/ "Part 2 - The Requirements")  

This week, I want to talk about the daily stand up - also known as the daily scrum, the scrum meeting, etc.  The daily stand up is a time boxed 15 minute meeting that occurs every day.  In the agile world time boxed means that the meeting length is maxed at the minutes set forth in the time box, but can be shorter.  The meeting should never go over the time box minutes.  This means our daily stand up should never go over 15 minutes.  Let's talk about the daily stand up.

####When
This meeting can occur at any point during the day.  Most agile teams hold the meeting first thing in the morning before the day starts or the last part of the day before everyone goes home.  By holding the meeting at one of these two points the daily stand up will not interfere with your development team's focus.  It also helps the development team set up the work for the day (if the meeting is in the morning) or their work for the next day (if it occurs at the end of the day)

####Who
This meeting is the for the development team.  The development team as defined by the [Scrum Guide](https://www.scrumguides.org/ "Scrum Guide") is a team made of members who have the skills necessary to create/develop a product.  They include programmers, DBAs, QA tester, UX designers, etc.  It does not include any managers, business stakeholders, or product owner.  It is important that the daily stand up be for the development team - and they should run and maintain the meeting.  Any outside influences can take away precious time the development team needs to discuss the development of the product.  This meeting is not meant to update any stakeholders or leadership - but a chance for the development team to get in sync with each other.  While people outside the development team can attend, they should only observe, never speak.

####What
The format of the daily stand up is really up to the development team.  Many agile teams follow a common format where the development team goes around the room with each member providing an update on what they accomplished since the last meeting, what they plan to accomplish before the next meeting, and any road blocks.  These updates should be short and succinct allowing the meeting to keep moving forward and to be completed within the 15 minute time box.  Any issues/roadblocks should be given to the scrum master who will handle them outside the meeting.  Unless the roadblock is within the development team and can be handled quickly in the daily stand up it should not be discussed here.  Roadblocks tend to not impact the entire team and can take up valuable time from the other members.  Most scrum masters will keep an impediment list where they can keep track of impediments and knock them off the list as they are removed.


