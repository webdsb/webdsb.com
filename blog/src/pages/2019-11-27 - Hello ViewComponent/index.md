---
path: "/hello-viewcomponent/"
date: "2018-11-27T23:15:33.962Z"
title: "The View Component in .NET CORE"
tags: Dot Net,ASP.NET MVC
---

With all the changes and new features in [.NET Core](https://docs.microsoft.com/en-us/dotnet/core/) it may be easy to have missed that we have to say goodbye to @Html.RenderAction() from within our views.

I have written a lot of code where my main view would call several renderactions to build out partial views.  In thinking about this, that is a very expensive call to simply render some partial views.  The complete web stack has to be created whenever we call an action and in the end we are simply just wanting to render a partial view and maybe pass it some viewmodel data.

####The ViewComponent
The ViewComponent makes perfect sense when we want to render a partial view with some data from the view model because it will only render the small chunk we need and not build out the complete request/response objects.  You would use a ViewComponent anywhere you might need a partial view that needs to retrieve some data to render properly.

A ViewComponent like a controller action consist of two parts, the ViewComponent and the View.

Creating ViewComponents is simple as well.  Inside your web project create a folder called ViewComponents then add a class.  You have two options at this point.
1) End your class name with ViewComponent
```
 public class MyClassViewComponent : ViewComponent
 {
        ...
        public IViewComponentResult Invoke(string parm1, int parm2, bool parm3)
        {
            ...
```
2) Add a view component attribute to a class name
```
[ViewComponent]
 public class MyClass : ViewComponent
 {
        ...
```

Then you would access the ViewComponent just like a controller, by referencing just the MyClass name, irregardless of which option you pick.  You can access your ViewComponent from your view using a tag helper:

```
<div>
   <vc: MyClass parm1="a" parm2="1" parm3="false">
</div>
```
We can pass parameters using an attribute on the tag helper that matches the parameter name on the ViewComponent.