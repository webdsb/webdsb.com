---
path: "/mongo-and-mvc-core/"
date: "2018-06-13T21:30:33.962Z"
title: "Connecting ASP.NET MVC and Mongo"
tags: Mongo,Dot Net,ASP.NET MVC
---

Mongo is an open source document database that uses JSON documents for a simple database design that can easily scale across servers.  Mongo is a NOSQL database with document stores whereas datatbases like Microsoft SQL Server, Teradata, and MySQL are relational and store data in a tabular format.  When it comes to the [CAP Theorem](  https://ai.google/research/pubs/pub45855 "CAP Theorem") Mongo leans toward Availability (all request will receive a response - no error state) and Partition Tolerance (Mongo will continue normal operations with N number of message failures).  With this Mongo does give up some consistency because data changes will eventually propagate to all nodes - eventually in a high performant system this eventual might take less than a few milliseconds.

So that's what mongo is, now the why?  Why might we want our .NET app to talk to a mongo database?  As we begin dealing with big data we also tend to get more complex data - data that won't fit nice and neatly into a row and column database.  If we do manage to fit the data into a row and column database we are stuck with multiple joins and hindered performance.  With a document data we can fit the frequently joined data into a single document and query it in a single lookup.  There will be plenty of future blog posts that go into more details on the why and what's of mongo - but now lets look at setting up a Mongo database that our MVC app can use.

I am a big fan of [mlab](https://mlab.com "MLAB Mongo") and there hosting plans.  You can even sign up for a free account that will give you a free sandbox of a half a gig - more than enough for a play area.  Once your project is ready for real time you can scale up your sandbox and pay per gig or you can pay a monthly fee to have a dedicated cluster in any of the popular cloud providers (Amazon, Google, or Azure).  You can use any mongo instance or if you are feeling adventurous you can even download the mongo source and host your own mongo instance locally - it is entirely opened source. We will play with a local instance later. For now all we need is a sandbox so I will build mine on mlab.

MLab takes all the set up work out of getting your mongo database up and running.  You simply pick your cloud service, your plan, and a region for your cloud service.

![New mongo DB](../images/2018-06-13/Mlab_Setup.png "New mongodb")

Since we will be working with our [simple mvc project](http://webdsb.com/mvc-configsettings/ "Simple MVC"), let's call our database simplemvcproject.

![New mongo DB name](../images/2018-06-13/Mlab_Setup2.png "New mongodb name")

To get any use out of our mongo db we will need to create a user.  I have created a simplemvcuser

![New mongo DB user](../images/2018-06-13/Mlab_Setup_User.png "New mongodb user")

Now we are ready to create our first collection...lets talk collections and documents.  If you wanted a simple comparison, a collection is the equivalent of a table in a relational database and the document would be similar to the data stored inside the table.  So a mongo database has collections, and a collection has documents.  Documents contain data stored in a special JSON format called BSON - binary representation of JSON.

![New mongo DB collection](../images/2018-06-13/Mlab_Setup_Collection.png "New mongodb collection")

With that we have a collection, but it is essentially an empty table - to add some data to our table let's create a document for our collection.  A document in mongo will look familiar to anyone who has used json:
```
{
    fielda : valuea,
    fieldb : valueb,
    fieldc : valuec,
    .
    .
    .
}
```
This is, in the most simplest comparison, creating a row and column relationship - similar to our table structure in a relational database.  So maybe our first document will be something simple.
```
{
    "_id": {
        "$oid": "5b207370fb6fc033f88339f2"
    },
    "name": "Cow",
    "colors": [
        "black",
        "white"
    ],
    "attributes": {
        "legs": 4,
        "tail": true
    }
}
```
Let's break this down...
```
"_id": {
        "$oid": "5b207370fb6fc033f88339f2"
    }
```
This is the primary index - in our case a GUID.  You can provide your own or most mongo implementations will generate a unique key on document insert if you completely leave off the _id field.  The mongo generated _id field will be based off a timestamp - and therefore can actually be used to generate a created on timestamp.

```
"name": "Cow",
```
Simple enough - a field called name and a string value of Cow.  The right side could be an integer (1,2,etc) or a date (new  Date(MMM DD, YYYY)).  
```
 "colors": [
        "black",
        "white"
    ],
```
This is the notation for an array or list of items.  You an easily query this by doing `Animal.find({colors.1})` which will return us white.  You can also query for a specific value in an array `Animal.find({colors:["black"]})`.  The array gives us great power in that we can store more data within our document - perhaps in the relational database world, the array may have spun off an additional table and required costly joins to retrieve the data.

```
"attributes": {
        "legs": 4,
        "tail": true
    }
```
This is equivalent to a C# class
```
public class attributes
{
    int legs;
    bool tail;
}
...
attributes.legs = 4;
attributes.tail = true;
```
As you will see in the next post, there are some handy .NET libraries that make mapping these mongo objects to classes very easy.

So that's it, a quick and fast primer on getting our first mongo collection and documents up and running.  The next mongo post will take our mongo database and see how to connect it with an MVC app.  In the meantime, go ahead and add more documents to your collection.