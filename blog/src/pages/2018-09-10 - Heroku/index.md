---
path: "/heroku/"
date: "2018-09-10T23:15:33.962Z"
title: "Hosting Python in Heroku"
tags: Python, The Cloud
---

This is the 2nd part of building our Alexa skill:  
[Part 1 -Alexa Skill](/alexa-skill/ "Part 1 - Alexa Skill")  


Last Week, we built an [Alexa Skill](/alexa-skill/ "Alexa Skill") and now we need a place to host it.  An easy and cheap way to get started is to pick one of the free or developer tiers of the major cloud providers, like [Azure](https://www.azure.microsoft.com "Azure") or [Amazon's AWS](https://www.aws.amazon.com "AWS").  Another option I have been playing with is [Heroku](https://www.heroku.com "Heroku").  Heroku is a Platform as a Service, PaaS that uses Amazon's AWS.  The advantage of a PaaS is that you focus more on deploying your application and less on any of the infrastructure.  Cloud providers like Azure and Amazon are Infrastructure as a Service and allow you to manage your servers, storage, and even networking abilities.  With a PaaS like Heroku you as the developer focus on the management of your application.  You still maintain the advantage of a cloud because you can scale, maintain high availability, and automate your deployments.  For development purposes there are two good reasons to pick Heroku, it has a nice free tier and it supports Python.

Once you have created an account with Heroku, you simply have to create an app...

![Create App ](../images/2018-09-06/Heroku_Create_App.png "Create App")

After you have created your application, you have a couple of choices for where you can store your application to deploy it.  If you use [GitHub](http://www.github.com "GitHub") you can link a GitHub repository and have every commit to a certain branch auto deploy to your heroku app.  Another option is to use a container and heroku's container CLI to deploy a full docker container to heroku.  For simplicity we are going to use the Heroku CLI and HerokuGIT to do our deployments.  This will allow us to connect to the heroku interface and deploy directly from our local development environment.  So download the CLI...

The first command is to login:
```
ps>heroku login
```

Now CD to your directory and follow the usual git commands

```
ps>git init
ps>heroku git:remote -a mytestskillapp
```

This is the typical [git workflow](http://webdsb.com/git-started/ "git workflow") of initializing your directory and adding a remote repository.  Take a minute now and make sure for your python application that you have created a requirements.txt file.  This file will tell python's PIP what libraries it needs to download.  My requirements.txt looks like this...

```
Flask==0.10.1
Flask-Script==2.0.5
flask-mysql==1.4.0
flask_ask
cryptography<2.2
```

Take special note of cryptography<2.2, there are some issues with the current version of PIP and cryptography versions greater than 2.3.  Now that your app is deployed, lets talk a little about the architecture of a Heroku.  All apps deployed to heroku are deployed into a container - the heroku containers are called dynos and they are virtual linux containers that will execute code based on the language (python, node, etc).  If you are using a production tier of heroku, it will auto scale up containers as needed - but in our case since we are on the free tier, we only get one.  One other thing to note, there are two types of dynos, a worker dyno and a web dyno and an application can consist of both kinds.  You may have your front end living on a web dyno while some backend database work is done on a worker dyno.  Alexa will communicate through a web call, so let's spin up our single web dyno.

```
heroku ps:scale web=1
```

That's it, you are all set.  You can click open app and verify the [Python App](/alexa-skill/ "Python App") we wrote last week is up and running.  You may want to run the command below to view the live logs as traffic hits your application.

```
heroku logs --tail
```
This leaves an open session to the logging, but only shows the most recent logs.  You can retrieve n logs by running, replace 100 with how many log statements you want to see.
```
heroku logs -n 100
```