---
path: "/gatsby-tags/"
date: "2018-08-29T23:15:33.962Z"
title: "Adding Tags to Gatsby"
tags: Gatsby,React
---

If you have decided to use a site generator like [Gatsby](/tag/gatsby/ "Gatsby") be prepared to create and implement features you get for free from blogging platforms like WordPress.  One of those features is tags or categories - for this post we will treat tags and categories the same - a keyword that can link all common blog post together.  For example this post and any other post about Gatsby might be file under a tag called [Gatsby](/tag/gatsby/ "Gatsby").

####Making sure your FrontMatter is set up correctly
Most html site generators share a common summation area at the top of the markdown called Front Matter - this is the place where you store information about the blog post.  It might include the path of the markdown, the date published, a title, and now in our case a tag.  It is usually located inside a set of ---.

```
---
path: "/gatsby-tags/"
date: "2018-08-29T23:15:33.962Z"
title: "Adding Tags to Gatsby"
tags: Gatsby, React
---
```
GatsbyJS uses graphQL to query the front matter in the markdown files.  In the simplest terms, a graphQL query is sent to an API and based on the inputs from the query you are returned a result set in JSON.  In fact the input query for graphQL looks very much like a JSON file itself.  Our Gatsby graphQL might look something like this...

```
 node {    
    frontmatter {
        date
        path              
        title 
        tags             
    }
}
```
Here the 'node' element is just the top layer that tells Gatsby I want to look for this in the markdown files.  While not important here, graphQL has the added benefit of being able to chain your calls together.  In traditional API calls you may have to make several rest or soap calls to get a collection of data.  GraphQL can be set up to chain references so it simply walks down references to get you all the data you need in a single call.

So back to Gatsby - inside our gatsby-node.js file we will need to add the tag to our graphQL schema.  My schema now looks like this...
```
return graphql(`
    {
      publishedPosts: allMarkdownRemark(
        sort: { order: DESC, fields: [frontmatter___date] }
        limit: 1000        
      ) {
        edges {
          next {
            frontmatter {
              path
            }
          }
          node {
            excerpt(pruneLength: 250)
            html
            id            
            frontmatter {
              date
              path              
              title 
              tags             
            }
          }
        }
      }
```
####More GraphQL
Now that our schema is set up, we need to do the actual query for the data - this is done inside the template page - if you use the default Gatsby structure this is found in templates->blog-template.js.  At the bottom you should have a graphQL query set up with the current post information, all you need to is add tags.
```
export const pageQuery = graphql`
  query BlogPostByPath($path: String!) {
    markdownRemark(frontmatter: { path: { eq: $path } }) {
      html
      frontmatter {
        date(formatString: "MMMM DD, YYYY")
        path
        title
        tags
      }
    }
  }
```

At the top of my template I have set up all my markdown data to be stored in a constant called post - ``` const post = data.markdownRemark;```.  Post now has two properties, post.html and post.frontmatter.  I can access my tag information for the post by post.frontmatter.tags.  

####Some React
Because Gatsby is developed in React, I won't just add my tags to my template, instead, let's create a tags component.  Under src->components create a javascript file called CommaSeparatedTags.js.  Copy the code below...
```
import React from 'react';
import GatsbyLink from 'gatsby-link';
import PropTypes from 'prop-types';

const CommaSeparatedTags = ({ tags }) => (
  <div className="tags">
    Tags:{' '}
    {tags &&
      tags.split(',').map((tag, index, array) => (
        <span key={index}>
          <GatsbyLink to={`/tag/${tag}/`}>{tag}</GatsbyLink>
          {index < array.length - 1 ? ', ' : ''}
        </span>
      ))}
  </div>
);

CommaSeparatedTags.propTypes = {
  tags: PropTypes.string,
};

export default CommaSeparatedTags;
```
This is a simple component and because I don't care about the any of the react lifecycle events or actual state I am creating this as a functional component.  There is probably a miniscule performance advantage by going with a functional component, but in all reality that is probably not noticeable.  My component will take a parameter of tags - which will be passed from the graphQL query and displayed as a URL.  Now head back over to your blog-template.js and you can add your component in any place you would like.
```
<CommaSeparatedTags tags={post.frontmatter.tags} />
```
The full code would then look like
```
return (
    <div className="blog-post-container">
     <Helmet title={`webdsb - ${post.frontmatter.title}`} />
   
      <div className="blog-post">
        <h1>{post.frontmatter.title}</h1>
        
        <BulletListTags tags={post.frontmatter.tags}/>
        
        <div
          className="blog-post-content"
          dangerouslySetInnerHTML={{ __html: post.html }}
        />
      </div>
      <CommaSeparatedTags tags={post.frontmatter.tags} />
    </div>
  );
}
```
Notice in this case I've created an extended component to hold the blog template - in this case I do care about state because every blog post is it's own page and it needs to manage it's individual state.

As always my blog code can all be found here on gitlab - [webdsb.com](https://gitlab.com/webdsb/SimpleMVCCore "Webdsb.com").

