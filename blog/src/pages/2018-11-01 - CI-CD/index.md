---
path: "/how-not-to-do-continuous-delivery/"
date: "2018-11-01T23:15:33.962Z"
title: "How Not To Do Continuous Delivery"
tags: Agile
---

Every product owner, product manager, business stakeholder, or customer loves the idea of a frequent product release.  The idea that you could have a new version every day, every week, every other week makes the marketing team excited.  Who wouldn't love having more frequent bug fixes or enhancements?  Continuous delivery does sound amazing, but it only works if you have the supporting pieces in place.  

In order to really have a solid continuous delivery process, you need to back it up with continuous integration (code is constantly delivered back to master branch), continuous build (as soon as change is added to master, the master branch is automatically compiled and built), continuous deployment (as soon as the build is successful, it is deployed to the test environment), automated testing (as soon as a change is detected in test, an automated test suite is kicked off), and finally upon successful testing, a build is deployed to prod.  Whew, that is a lot of infrastructure and code to build out...so we take shortcuts.

####Not Having Automated Deployments
Maybe having an automated process to copy build artifacts is too hard, so we automate the entire process until the end and we require someone to manually copy artifacts over to the production server.  We have gone through the entire process and have decided to leave the actual step of moving the code over to us error prone humans.  Sure manually copying artifacts over sounds easy, but it is ripe for error.  Most production applications sits behind a load balancer and are spread across multiple servers - so to manually deploy the applications means manually copying across multiple servers.  Have you ever copied the wrong files, missed files, or copied to the wrong directory?  It happens, it means less when you do it in your local or development server, but doing it in production can cause lots of issues. 

*Automate your deployments, it forces you to use a repeatable, proven process to ensure the right artifacts are moved to the right servers!*

####Saving integration until the end
The CI in CI/CD is continuous integration and it is an easy task to overlook.  How often have you waited until right before deployment to test (or worse, production) to do the final integration of all the development branches.  This is a disaster waiting to happen - merge conflicts, lost code, missed branches, are all possible outcomes. It is easy to think, "we only have a couple of developers...", but even today, merging a code file that has been worked on by two users in any complex system can be difficult and messy.  Anytime lines close to each other have changed, you have opened yourself up to possible lost code or merge errors.

*Integrate small changes, very frequently.  This way developers can pull changes down and integration can happen on both the master and child branches ensuring frequent testing.*

####Having Your Users Do Testing
Automated Testing is difficult and requires a commitment to keep it updated.  As features are requested and developed, the automated testing needs to run in parallel.  This takes resources.  If you are doing continuous delivery without automated testing you have assigned your users/customers to be de facto testers.  They are finding the bugs and reporting them through your support channels.  Manual testing works, but it is time consuming and is counter to a continuous delivery process because it slows down the entire process.  To do manual testing,  you have to freeze a build, do a deployment, and then have someone(s) take the time to manually walk through the application in a way they think best simulates a user.  Whether you do this or just a quick spot check test, you have ultimately outsourced regression testing to your users as they are the ones that will hit the edge cases and find your bugs.

*Automate testing and run it nightly.*

This is no way a complete list of doing Continuous Delivery correctly, but these are the items that seem to be skipped the most.  To do a successful continuous delivery process, we need to fully commit.  This may mean we work at it in stages, start with automated builds, work your way to continuous integration, and start building those automated tests!

