---
path: "/productive_team/"
date: "2018-05-30T18:30:33.962Z"
title: "Having a Productive Team"
tags: SoftwareDevelopment,Teamwork
---

Several weeks ago [Cory House](https://www.bitnative.com// "Cory House") had a great tweet to sum up the team aspect of software development.

![Cory House Tweet](../images/2018-05-30/tweet.png "Cory House Tweet")
> A successful development team is like a bees nest. Give them trust and autonomy to do their work, and they'll happily make you honey. 
> But if you continually mess with the nest or heavily dictate the process, you 
> risk reducing production and angering the bees.

I have had the privilege of working on and with some great development teams over the years and this quote sums up the most successful ones. What does a team that has been given this autonomy and trust look like?  How do they make you honey?  Let's talk about some practical and pragmatic approaches to what makes a good development team.


####Communicating
When a team communicates well within itself and with those in management it creates transparency.  There are no hidden agendas, questioned motives, or unresolved issues.  The team raises problems and concerns and in turn those in leadership help remove barriers, problems, and alleviate concerns.  Inside the team, each member can benefit from the same transparency - there are no questions of who is doing what or what is not getting done.  The team members can support each other and lift each other up because everyone is aware of what is going on.

####Taking Responsibility
Let's face it, programming is difficult.  We create problems, bugs, and issues with just a few lines of code.  We make mistakes and write bad code.  It is easy to try and pass the blame, but members within a productive team will take responsibility for their mistakes.  Because the team is communicating, everyone knows blame and insults won't come from our mistakes; instead  each team member grows from mistakes and taking responsibility for their work.  Teams can build trust with management when they take responsibility for issues and troubles that could hinder deadlines or create production issues.  Management can show trust back to team by doing same - taking responsibility for over managing or creating an environment that hinders the bees nest.

####Mentoring
A good  development team has members with a vast array of experience and skills.  You will have developers with years of experience and developers right out of school.  In addition to years experience you will have team members who are experienced more in server side or client side languages, in scripting, or devops - there are many skill sets that make up a developer.  These differing levels of experience opens up a great opportunity to mentor and be mentored.  A successful team will have a cycle of mentorship.  Team members lacking experience will reach out to senior members and senior members will reach out to junior members so all can share in the knowledge of the team.  A good mentoring cycle will benefit the team, not just the mentee.

####Code Reviews
A good development team participates in regular and consistent code reviews.  I won't spend time here, Ben Carden has written an [excellent code review series](http://becstudios.com/blog/code-reviews/ "Code Reviews")

####Agility
A good development team will constantly adjust and pivot on how the team functions.  Using good feedback loops, the team will adjust processes, workflows, and development methodologies to benefit the team and help produce a good product.  When something is not working, the team changes directions and finds a path that does work.  Fail fast and move on is the team's motto.  The team does not get stuck on old ways or "the way it has always been done".  Instead it is constantly improving and adjusting to improve.

####Fulfilling Work
As developers we won't always have the opportunity to work on new code or create cool functionality, but if team members are given the opportunity to work on tasks that they find fulfilling then they will shine.  It is a motivator and driver to completing work when one knows the work is for the greater good.  So when possible find specific work for team members that will give them a sense of purpose; it will make the days where the member is maintaining an old piece of code or debugging an old problem more productive.

####Celebrating Success and Failures
A productive team will have difficult sprints of work.  There will be days and weeks where a team is required to really drive hard to get a solution.  Many times the team will be successful - the hard work will produce a desired result.  This should be celebrated - maybe it's a day off, a monetary reward, or some other way to show appreciation.  Other times the team will drive equally hard and not meet the goal.  While it may seem strange to "celebrate the failure", the team worked hard and pushed forward - rewarding a team for their effort can produce a team that will continue to make honey.

This list is far from a complete list - there are other practices and idea that can contribute to a successful and productive team.  This is just a taste of some keys to help build a successful team - what other things would you add?