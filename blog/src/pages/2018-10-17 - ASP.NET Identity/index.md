---
path: "/aspnet_core_identity/"
date: "2018-10-17T23:15:33.962Z"
title: "ASP.NET Core Identity"
tags: Dot Net,ASP.NET MVC
---

ASP.NET Core 2.1 introduced the identity UI to replace the existing Identity framework you would use in both [Core](https://docs.microsoft.com/en-us/dotnet/core/) and [Framework](https://www.microsoft.com/net/download/dotnet-framework-runtime).  In the past when I wanted to spin up the identity code built in with MVC, you would end up with a project that looked a lot like this...

![solution_explorer](../images/identity/solution_explorer.png "solution_explorer")

In addition the default controller built by the scaffolding can be close to one thousand lines long before you add any customization.  For a lot of web applications the default identity project is more than enough because you plan to use SQL Server or Azure B2C for the store, the default login behavior is sufficient, and you don't need any two factor authentication.

For ASP.NET Core 2.1, things have greatly improved thanks to Razor UI Class Libraries.  The Razor UI class libraries allow you to build reusable UI components that include the UI pages, the controllers, the models, and the viewmodels all into a package you can reuse across projects.  These are then stored in an MVC area where you can use the current project's layout page and override views inside the library.  There is a lot of manual work in building these templates out, but to build your own, read more here: [Razor UI](https://blogs.msdn.microsoft.com/webdev/2018/03/01/asp-net-core-2-1-razor-ui-in-class-libraries/).

Now back to Identity, with Core 2.1, the Identity packaging has moved into a reusable Razor library.  By including the ASP.NET Core identity UI, our pre-built identity code looks much smaller...

![solution_explorer](../images/identity/solution_explorer_razor.png "solution_explorer")

The _ViewStart.cshtml tell the razor identity code the layout we want to use, so let's just point it to our current layout page.

```
@{
    Layout = "/Views/Shared/_Layout.cshtml";
}
```

Despite not having a controller, view, viewmodel, etc, we have a fully functional login page.

![login](../images/identity/login.png "login")

In addition to that, let's say we want to add Facebook login...normally you would need to touch a view and a controller at minimum, but now we simply need to register a social login in our startup.cs.  In the configure services, below your DefaultIdentity section add:

```
   services.AddAuthentication().AddFacebook(facebookOptions =>
            {
                facebookOptions.AppId = Configuration["AppId"];
                facebookOptions.AppSecret = Configuration["AppSecret"];
            });
```
Note you will need to register with Facebook to get your app id and app secret so your MVC app can authenticate with Facebook.  Now when we click on login, we should see the Facebook login button.

![login facebook](../images/identity/login_facebook.png "login facebook")

You might be asking, well how do I customize my identity views - it's very simple.  We can add our own customized identity views, just follow the naming convention (Login.cshtml, Register.cshtml, etc) and the built in controllers will look for custom ones first.  Let's add a really dumb login page.  Under Areas, Identity, Pages, add a new folder called "Account" and create a razor view called Login.cshtml.  

```
@page
@{
}

<h1>This is a custom login page, but it does nothing</h1>
```
Now when I click login I am directed to our custom login page.

![login custom](../images/identity/custom_login.png "login custom")

These updates to Asp.NET core identity allow us to have smaller projects, but maintain the same powerful prebuilt identity framework of old.  We will no longer have to deal with lots of files and extra lines of code when the default identity code is all we need.  In addition we can provide customizable UI experiences and still keep the controller and viewmodel work inside the class library.

