---
path: "/!important/"
date: "2018-12-20T23:15:33.962Z"
title: "I am !important"
tags: CSS,FrontEnd
---

Have you ever opened up a css file or two and seen something like this...

```
/*site.css*/
div > span {
   background-color: #707070;
}
...
/*page2.css*/
div > span !important {
   background-color: #FFF;
}

```

!important is valid CSS and has a place, but often times I've used it as a last heroic effort to get the my CSS selector to work because somewhere something is keeping this particular selector from working.  Unfortunately too many !importants can cause a site to be a nightmare to maintain and many times the !important is not actually needed.  The C in CSS is for cascading and the magic of the cascading can help us eliminate many of our !important tags.

Browsers uses specificity to know which CSS selector will be applied to the element.  This specificity is a hierarchal structure and if you find yourself adding a lot of !important then you may have a simple hierarchy issue.  The way a browser will read hierarchy is like this.
1. Inline Style
2. ID
3. Class/Attribute
4. Element/Pseudo element

To calculate the specificity simply add 1 for an element/Pseudo Element, add 10 for each Class/Attribute, 100 for each ID, and 1000 for each inline style.  Let's take an anchor tag and determine the specificity.

```
<a href="www.webdsb.com">Link</a>
```
The above has a specificity of 1 - it is a single element.
```
<div id="link_div">
   <a href="www.webdsb.com">Link</a>
</div>
...
/*style.css*/
#link_div a {}

```
This code has an ID so add 100 and an element so add 1

At this point the specificity would choose the bottom code as the style to apply because 101 is greater than 1.  So one more...
```
<div id="link_div">
   <a href="www.webdsb.com" style:"color:#707070">Link</a>
</div>
```
This has inline styling so we would add 1000 and this would be the style applied, even if our CSS in example 2 has different styling.  1000 is greater than 101.

Finally it is important to remember that if you duplicate a rule, the latter rule is considered closer to the element and will be the one applied.  So if you have the same rule in two CSS files, whichever file you include last will be the one that is applied.  This often bites us and is the reason we often use the !important.