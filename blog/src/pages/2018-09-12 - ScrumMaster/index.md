---
path: "/scrum_master/"
date: "2018-09-12T23:15:33.962Z"
title: "The Scrum Master"
tags: Agile
---

![Waterfall ](../images/waterfall.jpg "Waterfall")

If you cannot tell I love talking about [Agile development](/tag/Agile "Agile development").  When done correctly Agile can improve so much about the life cycle of software development.  You can produce a better quality product, with a more efficient team, in shorter release cycles, producing a happy customer.  Unfortunately in many shops Agile is only partially implemented or mixed in with waterfall and other company specific software development methodologies rendering it ineffective or to be simply used as a buzzword.  When agile is only partially implemented one of the first things to drop or to be altered is the Scrum Master.

####Who?
The Scrum Master is not an optional part of agile scrum process.  It can not be replaced or altered by another person - it is a vital role in the process.  According to the [scrum guide](https://www.scrumguides.org/scrum-guide.html "scrum guide") the Scrum Master is "responsible for promoting and supporting Scrum as defined in the Scrum Guide.  Scrum Masters do this by helping everyone understand Scrum theory, practices, rules and values."

As software developers we should not expect stakeholders or customers to understand our software development cycle.  In fact leadership and management **probably** shouldn't be required to fully understand all the nuances of agile or scrum.  This is where the Scrum Master comes in - the Scrum Master serves the development team by maximizing the value of the interactions outside individuals have with the development team.  This might mean reducing unneeded meetings or limiting low value interactions with members of the development team so they can focus on delivering the work items with the most priority.

In addition the Scrum Master can coach the development team on [agile practices](/tag/Agile "agile practices") while also working to remove impediments that might be in the teams way.  The coaching part means the Scrum Master is providing teaching and examples on how to best utilize scrum/agile and how to organize itself into a successful team.  This means that team celebrates it successes, but also organizes around problems and issues and does it's best to resolve those within the team.  When the team solves issues internally the impact is greater and more effective because it has limited outside influence.

####The Scrum Master Is Not...
As important as who the Scrum Master is, it is just important to define who a Scrum Master is not.  A Scrum Master should not be:
* A Manager who has direct reports on the development team
* A Project Manager
* A Product Owner
* A team member new to Scrum/Agile

The first three items above are not concrete requirements for agile, but they should be strongly considered when picking a Scrum Master.  If the Scrum Master is a manager who has direct reports on the development team this hinders retrospective.  The Scrum Master should be involved in the retrospective and if they have direct reports then the development team will not feel comfortable sharing problems or hindrances in the retrospective due to fear of retribution.  It is imperative that the development team feel comfortable going to the Scrum Master with impediments or issues that impact productivity without fear of management retribution.

The Scrum Master not being a Project Manager or Product Owner (PM/PO) is to help prevent conflicting duties.  The product owner/project manager first duty is to the product and stakeholders, not to the development team.  The PM and PO need to focus on requirements, backlog prioritization, and handling stakeholder expectation.  Once an individual tries to fill both roles, neither is handled well because they will be pulled in too many directions. 

Finally, the Scrum Master needs to be a person who has used agile - that has experience on a development team, has trained and learned agile standards.  They need to coach the team and lead them in following the agile manifesto.  This is difficult to impossible for someone who as little to no experience in an agile development environment.


