---
path: "/the-user-story/"
date: "2018-06-27T23:15:33.962Z"
title: "The User Story"
tags: Agile
---

This is the 2nd part of a series on agile...catch up here:  
[Part 1 -The User Story](/the-user-story/ "Part 1 - The User Story")  
[Part 2 - The Requirements](/the-requirements/ "Part 2 - The Requirements")  
[Part 3 - The Stand Up](/the-standup/ "Part 3 - The Standup")  

Agile, Scrum, Kanban, Iterative, Waterfall - we have all heard of, read about, and used the different software development methodologies.  While I tend to lean toward using the agile development practices, there is a key component to all software development practices - the user story.  The user story is ground floor for developing software - whether it is part of a Product Backlog Item (PBI) in agile or a stand alone work item in a waterfall world - it is the foundation in which a developer begins their work.

####The What
What is a user story?  A user story is not a feature, but a goal, or desire.  It is something the user of your application needs.  A user story is small - it won't tell the complete story of a feature, but it will tell the story a small component of a feature.  A feature may be made up of several user stories.  Another key to note is a user story is not a requirement - requirements list out the details of the request and put the application change into the center.  If we focus on the user story we focus on the user and thereby putting the user as the center of our requirement.  It is a small detail, but an important one - we develop software not for the software's sake, but for the end user.  The user story forces us to focus on the user.

####The Why
At first glance the user story may seem like just an extra step - one more thing needed.  In reality it is much more than that.  The requirements of old were often long list of what a piece of functionality needed to do.  Let's take a simple input screen and button - have you ever gotten a requirement that looked like this?
```
1) Place to input address
2) An "OK" button
3) A "Cancel" button.
4) Cancel button always enabled.
5) Ok button is enabled if there is text in the input box.
```

Simple enough, right?  Sure we could all write a program that does the above in just about any language - but what are we missing?  We don't understand what the user is doing with this.  Why does the user need this button and input, what are they doing before and after this?  There is a lot of important hidden information missing from this requirement.  What would a user story look like?

####The How
So how do you write a user story?  It is simple - both in writing and make up.  A user story should not be complicated and can follow a pretty simple template.
```
As/Being A <User Role/Title/etc>
 I need to <What they need>
 So I <Why do you need this>
 ```

 We have broken our user story into 3 parts - the person, the need, and the reason.  The person tells us the users of our system - this should be specific.  If it is a user who has Read/Write/Admin capabilities mention that - this also provides the basis for our security.   The need is the functionality or the request - what does the user want?  The last part may seem overkill, but we want to know the why behind the request.  The why can help us better understand the usefulness of the functionality and how to better build it.  So maybe our requirements above could be broken into

 ```
 As an administrator
 I need to enter customer addresses
 So I can keep records updated
 ```
 Are we missing any information?  Sure we have not talked about the color of the button or when it is enabled, text formats, etc.  What we have gained is more insight into our request.  The user is an administrator and they need to enter address information so they can update customer records.  We know our security, we know the need, and we know the why.  Now we can move to the requirement - we will talk about that next week.