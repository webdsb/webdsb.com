import React from 'react'
import PropTypes from 'prop-types'
import Helmet from 'react-helmet'
import GatsbyLink from "gatsby-link";
import Media from 'react-media'

// import Header from '../components/header'
import './index.css'
import '../styles/layout-overide.css';
import '../layouts/style.css'
import '../font-awesome/css/font-awesome.min.css'
import '../bootstrap.css'

const Header = () => (  
  <div className="container" >
  <div id="About" className="row">
    {/* <div id="Header"> */}
    <div id="logo" className="col-sm-8"><a href="/"><img src="/images/logo.png"/></a></div>  
    {/* </div>   */}
    <div className="col-sm-4">
    <ul id="top-social">
      <li ><GatsbyLink to={`/about`} className="icons"><i className="fa fa-info fa-2x" aria-hidden="true" title="About Me"></i></GatsbyLink></li>
      <li ><a href="https://www.twitter.com/webdsb" className="icons"><i className="fa fa-twitter fa-2x" aria-hidden="true" title="Twitter"></i></a></li>
      {/* <li><a href="https://www.linkedin.com/in/david-blackburn-4419b85" className="icons"><i className="fa fa-linkedin-square fa-2x" aria-hidden="true"></i></a></li> */}
      <li><a href="https://gitlab.com/webdsb" className="icons"><i className="fa fa fa-github fa-2x" aria-hidden="true" title="GitLab"></i></a></li>
    </ul>   
    </div>
            <div className="top-divider"></div>
  </div>  
  </div>
);

const Sidebar = (props) => (
  <div
      style={{
        border: '2px solid #e6e6e6',
        maxWidth: 960,
        padding: '0.5rem',
        marginBottom: '25px'
      }}
      >
      <strong>{props.title}.</strong> {props.description}
      <br />
      <GatsbyLink to={`/about`}>Read More...</GatsbyLink>
      {/* <a href="../about">Read More...</a> */}
      <br />
      
  </div>
  );

  const Footer = (props)=> (
    <div
    style={{
        textAlign:'center'
    }}
    >&copy;{(new Date().getFullYear())} webdsb.com|David Blackburn</div>
  );

  const TemplateWrapper = ({ children }) => (
    <div
    style={{
        margin: '20px 20px 20px 20px',
        padding: '10px 10px 10px 10px'
       
      }}>
      <Helmet
        title="webdsb.com | David Blackburn"
        meta={[
          { name: "description", content: "Sample" },
          { name: "keywords", content: "sample, something" }
        ]}
      />
      <Header />
      <div
        style={{
          margin: "0 auto",
          maxWidth: 980,
          display: "flex",
          flexDirection: "row",
          justifyContent: "space-between" ,
          
          
        }}
      >
        <Media query={{ maxWidth: 848 }}>
          {matches =>
            matches ? (
              <div
                style={{
                  margin: "0 auto",
                  maxWidth: 980,
                  display: "flex",
                  flexDirection: "row",
                  justifyContent: "space-between",
                  height: "100%",
                  padding: "25px"
                }}
              >
                <div style={{ flex: 1 }}>{children()}</div>
              </div>
            ) : (
              <div
                style={{
                  margin: "0 auto",
                  maxWidth: 980,
                  display: "flex",
                  flexDirection: "row",
                  justifyContent: "space-between",
                  height: "100%",
                  padding: "25px"
                }}
              >
                <div style={{ flex: 2.5, paddingRight: "30px" }}>
                  {children()}
                </div>
  <div style={{ flex: 1 }}>
                  {/*<PostLink /> <Sidebar
                    title="Codestack"
                    description="Articles on React and Node.js. All articles are written by Me. Fullstack Web Development."
                  /> */}
                  <Sidebar
                    title="About David"
                    description="I am a web developer specializing in C#/ASP.NET MVC and JavaScript based in Chattanooga, TN."
                  />
                </div>
              </div>
            )
          }
        </Media>
      </div>
      <Footer />
    </div>
    
  );

const Layout = ({ children, data }) => (
  <div
  style={{
        margin: '10px auto',
        maxWidth: 960,
        padding: '0px 1.0875rem 1.45rem',
        paddingTop: 0,
      }}
      >
    <Helmet
      title={data.site.siteMetadata.title}
      meta={[
        { name: 'description', content: 'Sample' },
        { name: 'keywords', content: 'sample, something' },
      ]}
      
    />
    <Header siteTitle={data.site.siteMetadata.title} />
    <div
      style={{
        margin: '0 auto',
        maxWidth: 960,
        padding: '0px 1.0875rem 1.45rem',
        paddingTop: 0,
      }}
    >
      {children()}
    </div>
  </div>
)

TemplateWrapper.propTypes = {
  children: PropTypes.func
};

export default TemplateWrapper;

// Layout.propTypes = {
//   children: PropTypes.func,
// }

// export default Layout


