---
path: "/time_and_quality/"
date: "2018-09-17T23:15:33.962Z"
title: "Time vs Quality"
tags: Musings
---

![Clock](../images/2018-09-12/Clcok.jpg "Clock")

If you have been developing for any amount of time you have probably run into the question of, do I get it done and **potentially** sacrifice quality or do I make it have good quality and **potentially** delay delivery.  It is a difficult place to come to as a developer, deadlines matter, but producing a good quality product that is bug free and easily maintained matters just as much.

####Quality
We can define two types of quality.  There is quality in code output, that is few to no bugs.  There is also quality in code readability and maintainability.  Developing high quality code takes time.  There is effort spent in properly unit testing the code, there is time in following design patterns and proper code structures.  Throwing code together can be done fairly quickly, but will most of the time end up with a high bug count and makes adding new features difficult and time consuming.  

At some point a developer has to find the balance of providing a high quality product and timely delivery.  We can put into estimates the time to design properly and even unit testing, but when it comes to creating a working product we need to make sure not to impact deadlines with too much design and theory.

####Time
Time is limited by our deadline.  It is a fact of life that some products just have deadlines and need to be delivered by certain dates.  While time should not drive our design and quality, it should factor into it.  The stakeholder pays for a product and expect a feature within some time frame - this is not asking for too much.  

While time is defined, what is delivered within a time frame can be negotiated.  If we can shrink down the development scope and deliver a smaller feature of a larger [product backlog item](/tag/Agile) then we can focus on our quality and unit testing at a smaller scope.  




