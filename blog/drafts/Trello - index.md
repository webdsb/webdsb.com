---
path: "/agile-blog-development/"
date: "2018-06-06T21:15:33.962Z"
title: "Blogging with Agility"
tags: Musings,Blogging,Agile
---

###Blogging

I am new to blogging and want to keep a consistent blogging schedule, while also maintaining regular site updates.  To do this I plan to run weekly blog sprints.  Since this blog is not a full time job, what does a weekly blog sprint look like?

####Trello
I plan to run my site using a [kanban](https://en.wikipedia.org/wiki/Kanban/ "Kanban") methodology; I plan to write more about kanban later, but for now and at a thousand foot view, kanban is simply an agile way to develop software.  I have set up two kanban boards on [Trello](https://trello.com/ "Trello").  

One board is for posts:
![Blog Post Board](../images/2018-06-06/Blog_Post_Board.png "Blog Post Board")

Using this board, the ideas card can be a quick place to jot notes and blog post ideas.  They can be as short as a single word or a more detailed list of notes.  Once I decide to start working on an idea I move it to the drafts swim lane.

![Blog Post Board](../images/2018-06-06/Blog_Post_To_Draft.png "Blog Post Board")

Once a card is in this lane, I will actually start writing out the draft - this includes both the blog text, research, and the markdown to style the post.  The ideas and drafts swim lanes can have multiple cards in them as ideas come in and drafts are being worked.  Once a post feels completed, I will move it to the Testing lane.

![Blog Post Board](../images/2018-06-06/Blog_Post_To_Test.png "Blog Post Board")

A post in the testing lane will be reviewed for any issues both in the blog text and in the layout of the page.  Any failures will result in the post moving back to Drafts; successes will move forward to published.

![Blog Post Board](../images/2018-06-06/Blog_Post_To_Publish.png "Blog Post Board")

A post in the published lane will be published with the next build of the site.

###Pomodoro Technique
The [Pomodoro Technique](https://francescocirillo.com/pages/pomodoro-technique "Pomodoro") is a time management strategy that uses a tomato time to break work down into "pomodoros" or 25 minute work sprints.  After a 25 minute work sprint you take a 5 minute break.  Once you have completed 4 pomodoros you take a 15 minute break.  The idea behind a pomodoro is that you can focus full on for 25 minutes and ignore any interruptions.  During a pomodoro you will make note of any interruptions and determine how you can limit and reduce those in the future.  If you are unable to ignore an interruption you must abandon the pomodoro and resume your count with the next full pomodoro.  The name pomodoro comes from the low tech tomato timers the inventor of the method used to time his pomodoros.  Not wanting to carry a tomato timer I have used the very simple [Marinara Timer](https://www.marinaratimer.com/IiPyOIM7 "Marinera Timer").  It is a simple timer, that is free and browser based so it works on all computers and mobile devices.

###Fail Fast and Adapt
Finally I subscribe to a fail fast methodology.  If a post isn't working out, or a layout change doesn't look right, I move on and adjust.  Perhaps a post just isn't coming together, it can stay in the draft lane or move back to the ideas lane.  If a site layout change does not look or feel right, roll back and change.  The beauty of an agile approach is there is always another sprint.